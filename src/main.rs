use image::ColorType;
use raqote::*;

use nalgebra::Point2;
use worldgen::hex::Hex;
use worldgen::noise::SimplexNoise;

fn main() {
    let mut dt = DrawTarget::new(512, 512);
    let noise = SimplexNoise::new(&mut rand::thread_rng());

    for x in -20..20 {
        for z in -20..20 {
            let hex = Hex::from_axial(x, z);
            let a = hex.vertex(25.0, 0);
            let mut pb = PathBuilder::new();
            pb.move_to(a.x, a.y);
            for i in 0..6 {
                let b = hex.vertex(25.0, i + 1);
                pb.line_to(b.x, b.y);
            }
            pb.close();

            dt.stroke(
                &pb.finish(),
                &Source::Solid(SolidSource {
                    r: 0x0,
                    g: 0x0,
                    b: 0x80,
                    a: 0xff,
                }),
                &StrokeStyle {
                    width: 2.0,
                    ..Default::default()
                },
                &DrawOptions::new(),
            );

            let center = hex.to_world(25.0);
            let value = (noise.get(center / 100.0) + 1.0) * 255.0;
            let sx = value / 255.0 - 1.0;
            let sy = value % 255.0 / 255.0 - 0.5;
            let mut pb = PathBuilder::new();
            pb.arc(
                center.x + sx * 10.0,
                center.y + sy * 10.0,
                2.0,
                0.0,
                std::f32::consts::PI * 2.0,
            );

            dt.fill(
                &pb.finish(),
                &Source::Solid(SolidSource {
                    r: 0x0,
                    g: 0x0,
                    b: 0x80,
                    a: 0xff,
                }),
                &DrawOptions::new(),
            );
        }
    }

    dt.write_png("test.png").unwrap();

    let mut buf = vec![0; 512 * 512];

    for x in 0..512 {
        for y in 0..512 {
            let mut sum = 0.0;
            let mut freq = 1.0;
            let mut ampl = 1.0;
            let mut max = 0.0;
            for _ in 0..8 {
                let pt = Point2::new(x as f32, y as f32) / 400.0 * freq;
                sum += ampl * noise.get(pt);
                freq *= 2.0;
                max += ampl;
                ampl *= 0.5;
            }
            buf[y * 512 + x] = ((sum + 1.0) * 255.0 * 0.5 / max) as u8;
        }
    }

    image::save_buffer("noise.png", &buf, 512, 512, ColorType::L8).unwrap();
}
