use rand::seq::SliceRandom;
use rand::Rng;

use nalgebra::Point2;

fn grad(hash: u8, x: f32, y: f32) -> f32 {
    const GRAD: [[f32; 2]; 12] = [
        [1.0, 1.0],
        [-1.0, 1.0],
        [1.0, -1.0],
        [-1.0, -1.0],
        [1.0, 0.0],
        [-1.0, 0.0],
        [1.0, 0.0],
        [-1.0, 0.0],
        [0.0, 1.0],
        [0.0, -1.0],
        [0.0, 1.0],
        [0.0, -1.0],
    ];

    let [ax, ay] = GRAD[(hash % 12) as usize];
    x * ax + ay * y
}

#[derive(Clone)]
pub struct SimplexNoise {
    perm: Box<[u8; 512]>,
}

impl SimplexNoise {
    /// Create a random perlin noise generator
    pub fn new<R: Rng>(rng: &mut R) -> SimplexNoise {
        let mut perm = Box::new([0; 512]);
        for i in 0..255 {
            perm[i as usize] = i;
        }

        perm[0..255].shuffle(rng);
        perm.copy_within(0..255, 256);

        SimplexNoise { perm }
    }

    /// Get noise value
    pub fn get(&self, pos: Point2<f32>) -> f32 {
        const F2: f32 = 0.36602542;
        const G2: f32 = 0.21132487;

        let s = (pos.x + pos.y) * F2;
        let i = (pos.x + s).floor();
        let j = (pos.y + s).floor();
        let t = (i + j) * G2;
        let x0 = pos.x - (i - t);
        let y0 = pos.y - (j - t);

        let (i1, j1) = if x0 > y0 { (1, 0) } else { (0, 1) };

        let x1 = x0 - (i1 as f32) + G2;
        let y1 = y0 - (j1 as f32) + G2;
        let x2 = x0 - 1.0 + 2.0 * G2;
        let y2 = y0 - 1.0 + 2.0 * G2;

        let ii = ((i as i32) & 255) as usize;
        let jj = ((j as i32) & 255) as usize;
        let gi0 = self.perm[ii + self.perm[jj] as usize];
        let gi1 = self.perm[ii + i1 + self.perm[jj + j1] as usize];
        let gi2 = self.perm[ii + 1 + self.perm[jj + 1] as usize];

        let mut t0 = 0.5 - x0 * x0 - y0 * y0;
        let n0 = if t0 < 0.0 {
            0.0
        } else {
            t0 *= t0;
            t0 * t0 * grad(gi0, x0, y0)
        };

        let mut t1 = 0.5 - x1 * x1 - y1 * y1;
        let n1 = if t1 < 0.0 {
            0.0
        } else {
            t1 *= t1;
            t1 * t1 * grad(gi1, x1, y1)
        };

        let mut t2 = 0.5 - x2 * x2 - y2 * y2;
        let n2 = if t2 < 0.0 {
            0.0
        } else {
            t2 *= t2;
            t2 * t2 * grad(gi2, x2, y2)
        };

        70.0 * (n0 + n1 + n2)
    }
}
